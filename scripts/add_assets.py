import os
import argparse
import requests
import json

url = "http://127.0.0.1:8000/assets/"


def post_(orders):
    try:
        response = requests.post(url=url, json=orders)
        print(response.json(), "\n\n")
    except Exception as e:
        print("EXCEPTION", e)


def first2(x):
    return int(x[0:2])


def open_files(path):
    sorted_file_names = sorted(os.listdir(path), key=first2)
    print(path)
    for data_file in sorted_file_names:
        print(data_file)
        data_file = f"{path}/{data_file}"
        with open(data_file, "r") as json_file:
            file = json.load(json_file)
            post_(file["orders"])
            # print(file["orders"])


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Path you want to post")
    parser.add_argument("--path", type=str, required=False)
    args = parser.parse_args()
    path = args.path

    path = "json_resources/" + path
    open_files(path)
