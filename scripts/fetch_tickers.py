import requests


def fetch(asset_type):
    try:
        data = requests.get(f"https://mfinance.com.br/api/v1/{asset_type}").json()
    except Exception:
        raise ImportError(f'Import error fetching {asset_type}')
    tickers = [asset.get('symbol') for asset in data[asset_type]]
    print(tickers)


if __name__ == "__main__":
    fetch('fiis')
    fetch('stocks')

