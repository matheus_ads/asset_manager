import pytest
import mongoengine as me
from database.models import Asset
from database.schemas import AssetSchema
from datetime import date


@pytest.fixture(scope='function')
def mongo(request):
    db = me.connect('testdb', host='mongodb://localhost')
    yield db
    db.drop_database('testdb')
    db.close()


@pytest.fixture
def basic_fund():
    order_date = date(2019, 1, 21)
    fund = Asset(ticker='HGLG11',
                 order_price=144.34,
                 asset_type="Fii",
                 order_type='Buy',
                 quantity=4,
                 subscription=False,
                 order_date=order_date,
                 ceiling_price=180.0)
    return fund


@pytest.fixture
def alzr_basic_fund():
    order_date = date(2019, 1, 21)
    fund = Asset(ticker='ALZR11',
                 order_price=124.34,
                 asset_type="Fii",
                 order_type='Buy',
                 quantity=2,
                 subscription=False,
                 order_date=order_date,
                 ceiling_price=130.0,
                 base_date=14)
    return fund


@pytest.fixture
def hglg_data_buy():
    data = {
     "ticker": "HGLG11",
     "order_price": 137.31,
     "order_type": "Buy",
     "quantity": 3,
     "subscription": False,
     "order_date": "2020-01-08",
     "ceiling_price": "175.00",
     "base_date": 31
    }
    schema = AssetSchema()
    data = schema.load(data)
    return data


@pytest.fixture
def hglg_data_sell():
    data = {
     "ticker": "HGLG11",
     "order_price": 145.31,
     "order_type": "Sell",
     "quantity": 2,
     "subscription": False,
     "order_date": "2020-08-06"
    }
    schema = AssetSchema()
    data = schema.load(data)
    return data


@pytest.fixture
def hglg_buy_json():
    return {
     "ticker": "HGLG11",
     "order_price": 165.31,
     "order_type": "Buy",
     "quantity": 2,
     "subscription": False,
     "order_date": "2020-08-06"
    }
