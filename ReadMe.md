Some rules

Unit test needs a mongodb up(to mock)

I force data to be list to be easy add in batch so will work with [{..data..}] or just {..data..}

Ex:
Insert new real state trade:
{
    "ticker": "HGLG11", //required
    "order_price": 142.97, //required
    "order_type": "Buy", //required
    "quantity": 8, //required
    "subscription": true,
    "order_date": "03/07/2019", //required
    "ceiling_price": "140.00",
    "base_date": 31
}

Order Type only accept Buy or Sell
If subscription is empty then this field will be set to False
If subscription is True didn't add calculated taxes
Calculated taxes are being added to the average price
Json resource files should be closer to notes as possible to fast consulting and to add in batch 
Order date uses brazilian format, dd/mm/yyyy, I treat it in loads and dumps, but saved in UTC format ignoring time

GET API FILTER
pass through body request
{"field": "ticker", "op": "contains", "value": "H"}
{"field": "order_price", "op": "gte", "value": "110.0"}
to delete last consider just pass value with ticker name

field = any field of assets
op = operator in mongoengine, it will be concatenate with field and underscore like "ticker__contains"
value = for what we are looking
https://docs.mongoengine.org/guide/querying.html
