import pytest
from decimal import Decimal
from api.utils import calc_new_avg_price, calc_liq_tax, calc_emol_tax,\
    calc_income_tax, _choose_tax_by_date
from datetime import date


@pytest.mark.parametrize("old_avg, old_qty, new_price, new_qty, expected",
                         [(132.56, 8, 156.32, 3, 139.04), (220.47, 31, 272.31, 7, 230.01),
                          (146.77, 7, 130.22, 3, 141.80)])
def test_calc_new_avg_price(old_avg, old_qty, new_price, new_qty, expected):
    assert calc_new_avg_price(old_avg, old_qty, new_price, new_qty) == expected


@pytest.mark.parametrize("price, qty, expected", [(Decimal(144.75), 8, Decimal(0.31)),
                                                  (Decimal(104.82), 1, Decimal(0.02)),
                                                  (Decimal(220.12), 42, Decimal(2.54))])
def test_calc_liq_tax(price, qty, expected):
    assert calc_liq_tax(qty, price) == expected


@pytest.mark.parametrize("price, qty, expected", [(Decimal(144.75), 8, Decimal(0.04)),
                                                  (Decimal(104.82), 1, 0),
                                                  (Decimal(220.12), 42, Decimal(0.37))])
def test_calc_emol_tax(price, qty, expected):
    order_date = date(2019, 1, 3)
    assert calc_emol_tax(qty, price, order_date) == expected


@pytest.mark.parametrize("old_avg, old_qty, new_price, new_qty, tax, emol, expected",
                         [(Decimal(132.60), 8, Decimal(156.32), 3, Decimal(0.12), Decimal(0.01), Decimal(139.08)),
                          (Decimal(220.53), 31, Decimal(272.31), 7, Decimal(0.52), Decimal(0.07), Decimal(230.08)),
                          (Decimal(146.81), 7, Decimal(130.22), 3, Decimal(0.10), Decimal(0.01), Decimal(141.84))])
def test_calc_new_avg_with_taxes(old_avg, old_qty, new_price, new_qty, tax, emol, expected):
    assert calc_new_avg_price(old_avg, old_qty, new_price, new_qty, tax, emol) == expected


@pytest.mark.parametrize("new_price, new_qty, tax, emol, expected",
                         [(Decimal(156.32), 3, Decimal(0.12), Decimal(0.01), Decimal(156.36)),
                          (Decimal(132.07), 7, Decimal(0.25), Decimal(0.03), Decimal(132.10))])
def test_calc_first_avg_with_taxes(new_price, new_qty, tax, emol, expected):
    old_qty = old_avg = Decimal(0)
    assert calc_new_avg_price(old_avg, old_qty, new_price, new_qty, tax, emol) == expected


@pytest.mark.parametrize("quantity, price, expected",
                         [(20, Decimal(94.01), Decimal(0.09)),
                          (30, Decimal(116.95), Decimal(0.17))])
def test_calc_income_tax(quantity, price, expected):
    assert calc_income_tax(quantity, price) == expected


def test_choose_tax_by_date():
    old_date = date(2019, 3, 1)
    new_date = date(2020, 2, 2)
    expected_old = Decimal(0.0000411)
    expected_new = Decimal(0.00003125)
    assert _choose_tax_by_date(old_date) == expected_old
    assert _choose_tax_by_date(new_date) == expected_new
    assert not _choose_tax_by_date(old_date) == expected_new
    assert not _choose_tax_by_date(new_date) == expected_old
