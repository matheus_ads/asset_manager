from database.models import Asset


def test_save_fund(mongo, basic_fund):
    basic_fund.save()
    assert len(Asset.objects()) == 1
    assert Asset.objects.first().ticker == 'HGLG11'


def test_if_exists_fund(mongo, basic_fund):
    basic_fund.save()
    assert Asset.objects.exists('HGLG11')


def test_if_not_exists_fund(mongo):
    assert not Asset.objects.exists('HGLG11')
    assert not Asset.objects.exists('ALZR11')
