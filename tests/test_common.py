import pytest
from api.common import get_asset_type


@pytest.mark.parametrize("ticker, expected",
                         [("ITSA4", "Stock"), ("PETR4", "Stock"), ("MGLU3", "Stock"),
                          ("XPML11", "FII"), ("IRDM11", "FII"), ("VISC11", "FII"),
                          ("ABCD3", "")])
def test_get_asset_type(ticker, expected):
    assert get_asset_type(ticker) == expected
