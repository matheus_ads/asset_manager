import pytest
from decimal import Decimal
from datetime import date
from database.models import Asset
from api.handler import process_post, get_attr_value
from api.utils import calc_liq_tax, calc_emol_tax, calc_income_tax


def test_sell_operation_taxes():
    price = Decimal(116.95)
    quantity = 30
    order_date = date(2019, 1, 3)
    tax_expected = Decimal(0.96)
    emol_expected = Decimal(0.14)
    income_expected = Decimal(0.17)
    assert calc_liq_tax(quantity, price) == tax_expected
    assert calc_emol_tax(quantity, price, order_date) == emol_expected
    assert calc_income_tax(quantity, price) == income_expected


def test_process_post_buy(mongo, hglg_data_buy):
    avg_price_expected = Decimal(137.35)
    fund_id = process_post(hglg_data_buy)
    assert fund_id
    assert Asset.objects.exists('HGLG11')
    assert Asset.objects.latest('HGLG11').id == fund_id
    assert Asset.objects.latest('HGLG11').avg_price == pytest.approx(avg_price_expected)
    process_post(hglg_data_buy)
    assert len(Asset.objects) == 2


def test_process_post_complete(mongo, hglg_data_buy, hglg_data_sell):
    """Test insert buy and sell in sequence"""
    income_tax_expected = Decimal(0.01)
    avg_price_wtax_expected = Decimal(137.31)
    avg_price_expected = Decimal(137.35)
    fund_id = process_post(hglg_data_buy)
    assert fund_id

    fund_id_sell = process_post(hglg_data_sell)
    assert fund_id_sell

    assert Asset.objects.latest('HGLG11').avg_price == pytest.approx(avg_price_expected)
    assert Asset.objects.latest('HGLG11').avg_price_wtax == pytest.approx(avg_price_wtax_expected)
    assert Asset.objects.latest('HGLG11').current_total_quantity == 1
    assert Asset.objects.latest('HGLG11').income_tax == pytest.approx(income_tax_expected)
    assert len(Asset.objects) == 2


def test_get_no_attr_value(basic_fund):
    assert get_attr_value(basic_fund, None, 'base_date') == 0


def test_get_attr_value(basic_fund, alzr_basic_fund):
    ceiling_price = get_attr_value(basic_fund, alzr_basic_fund, 'ceiling_price')
    assert ceiling_price == Decimal(180.0)


def test_get_latest_value(basic_fund, alzr_basic_fund):
    latest_value = get_attr_value(basic_fund, latest=alzr_basic_fund, attr_name='base_date')
    assert latest_value == 14
