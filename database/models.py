from mongoengine import *


class AssetQuerySet(QuerySet):

    def latest(self, ticker):
        """Return latest inserted asset if exists"""
        return self.filter(ticker=ticker).order_by('-id').first()

    def exists(self, ticker):
        return self.filter(ticker=ticker)


class Asset(Document):
    meta = {'queryset_class': AssetQuerySet}

    ticker = StringField(required=True)
    asset_type = StringField(required=True)
    order_price = DecimalField(required=True, force_string=True)
    order_type = StringField(choices=("Buy", "Sell"), required=True)
    quantity = IntField(required=True)
    subscription = BooleanField(required=False)
    order_date = DateField(required=True)
    emoluments = DecimalField(required=False, force_string=True)
    tax = DecimalField(required=False, force_string=True)  # liq tax
    income_tax = DecimalField(required=False, force_string=True)  # IR
    avg_price = DecimalField(required=False, force_string=True)  # include taxes
    base_date = IntField(required=False)  # last day to get dividend
    ceiling_price = DecimalField(required=False, force_string=True)
    avg_price_wtax = DecimalField(required=False, force_string=True)  # without taxes
    current_total_quantity = IntField(required=False)  # current quantity sum of this asset
    brokerage = DecimalField(required=False, force_string=True)  # corretagem
    taxes = DecimalField(required=False, force_string=True)  # impostos

    def __repr__(self):
        return "Ticker {}," \
               "Quantity {}" \
               "Avg_Price {} " \
               "Date {}".format(self.ticker, self.quantity, self.avg_price, self.order_date)


class Dividend(Document):
    ticker = StringField(required=True)
    payment_date = DateTimeField(required=True)
    dividend_value = DecimalField(required=True)
    note = StringField(required=False)
    my_yield = DecimalField(required=False)
    current_yield = DecimalField(required=False)
