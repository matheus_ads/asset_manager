from marshmallow import Schema, fields, validate


class AssetSchema(Schema):
    ticker = fields.String(required=True)
    asset_type = fields.Str(validate=validate.OneOf(["FII", "Stock"]))
    order_price = fields.Decimal(required=True, as_string=True)
    order_type = fields.Str(validate=validate.OneOf(["Buy", "Sell"]))
    quantity = fields.Integer(required=True)
    subscription = fields.Boolean(required=False)
    order_date = fields.Date(format="%d/%m/%Y", required=True)
    emoluments = fields.Decimal(required=False, as_string=True)
    tax = fields.Decimal(required=False, as_string=True)
    income_tax = fields.Decimal(required=False, as_string=True)
    avg_price = fields.Decimal(required=False, as_string=True)
    base_date = fields.Integer(required=False)
    ceiling_price = fields.Decimal(required=False, as_string=True)
    avg_price_wtax = fields.Decimal(required=False, as_string=True)  # without taxes
    current_total_quantity = fields.Integer(required=False)
    brokerage = fields.Decimal(required=False, as_string=True)
    taxes = fields.Decimal(required=False, as_string=True)
