from flask import Flask
from database.db import initialize_db
from api.app import asset_bp

app = Flask(__name__)
app.config['MONGODB_SETTINGS'] = {
    'host': 'mongodb://localhost/assets'
}

app.register_blueprint(asset_bp)
initialize_db(app)

app.run(debug=True, port=8000)
