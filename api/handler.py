from decimal import Decimal
from database.models import Asset
from api.utils import calc_income_tax, calc_emol_tax, calc_liq_tax, \
    calc_new_avg_price
from api.common import get_asset_type

ORDER_TYPE_BUY = 'Buy'


def process_post(data):
    ticker = data.get('ticker')
    asset_type = get_asset_type(ticker)
    quantity = data.get('quantity')
    order_price = data.get('order_price')
    order_type = data.get('order_type')
    subscription = data.get('subscription', False)
    order_date = data.get('order_date')
    tax = Decimal(0)
    emol = Decimal(0)

    if subscription is False:
        tax = calc_liq_tax(quantity, order_price)
        emol = calc_emol_tax(quantity, order_price, order_date)

    asset = Asset(ticker=ticker, asset_type=asset_type, order_price=order_price, order_type=order_type,
                  quantity=quantity, subscription=subscription, order_date=order_date, emoluments=emol, tax=tax)

    latest = Asset.objects.latest(ticker)
    asset.base_date = get_attr_value(asset, latest, 'base_date')
    asset.ceiling_price = get_attr_value(asset, latest, 'ceiling_price')

    if data.get('order_type') == ORDER_TYPE_BUY:
        process_buy_asset(asset, latest)
    else:  # sell order
        process_sell_asset(asset, latest)

    asset.save()
    return asset.id


def process_buy_asset(asset, latest):
    if Asset.objects.exists(asset.ticker):
        old_avg_price = latest.avg_price
        old_avg_price_wtax = latest.avg_price_wtax
        old_qty = latest.current_total_quantity

        asset.current_total_quantity = latest.current_total_quantity + asset.quantity
        asset.avg_price = calc_new_avg_price(old_avg_price, old_qty, asset.order_price, asset.quantity, asset.tax,
                                             asset.emoluments)
        asset.avg_price_wtax = calc_new_avg_price(old_avg_price_wtax, old_qty, asset.order_price, asset.quantity)
    else:  # first order
        asset.avg_price = calc_new_avg_price(Decimal(0), Decimal(0), asset.order_price, asset.quantity, asset.tax,
                                             asset.emoluments)
        asset.avg_price_wtax = asset.order_price
        asset.current_total_quantity = asset.quantity


def process_sell_asset(asset, latest):
    asset.income_tax = calc_income_tax(asset.quantity, asset.order_price)
    asset.avg_price = latest.avg_price
    asset.avg_price_wtax = latest.avg_price_wtax
    if latest.current_total_quantity < asset.quantity:
        raise Warning('You cant sell more than you have')
    asset.current_total_quantity = latest.current_total_quantity - asset.quantity


def get_attr_value(asset, latest, attr_name):
    """function to get value from an attr if exists, else try in the latest obj
        if not found return 0
        used for get base_date and ceiling_price without broken in dict.get
       :param asset: current asset to be insert
       :param latest: Asset
       :param attr_name: attr to check
       :return: value found
       """
    result = getattr(asset, attr_name, 0)
    if result:
        return result
    elif latest is not None:
        return getattr(latest, attr_name)
    else:
        return 0
