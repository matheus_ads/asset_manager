from flask import Blueprint
from flask_restful import Api
from api.resources.assets import Status, AssetApi

asset_bp = Blueprint('assets', __name__)
api = Api(asset_bp)

# Routes
api.add_resource(Status, '/status/')
api.add_resource(AssetApi, '/assets/')
