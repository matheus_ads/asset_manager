import time
from flask import jsonify, request
from flask_restful import Resource
from database.models import Asset
from database.schemas import AssetSchema
from api.handler import process_post
from werkzeug.exceptions import BadRequest


class Status(Resource):
    @staticmethod
    def get():
        return jsonify(message="Everything Ok", status_code=200)


class AssetApi(Resource):
    @staticmethod
    def post():
        try:
            json_data = request.get_json()
        except BadRequest:
            return jsonify(message="No input data provided", status_code=400)

        if not isinstance(json_data, list):  # force many
            json_data = [json_data]

        asset_schema = AssetSchema(many=True)
        try:
            data = asset_schema.load(json_data)
            message = ""
            for item in data:
                asset_id = process_post(item)
                result = [Asset.objects().with_id(asset_id)]
                result = asset_schema.dump(result)
                message += f"New trade created {result} \n"
            return jsonify(message=message, status_code=200)
        except Exception as err:
            message = f"Error in insert an order: {err}"
            return jsonify(message=message, status_code=500)

    @staticmethod
    def get():
        try:
            json_data = request.get_json()
            if json_data:
                filters = _convert_filters(json_data)
                asset = Asset.objects().filter(**filters)
        except BadRequest:
            asset = Asset.objects()
        asset_schema = AssetSchema(many=True)
        result = asset_schema.dump(asset)
        return {"quantity": len(result), "assets": result}, 200

    @staticmethod
    def delete():
        """Just to delete last added asset
            force value param to be ticker name
        """
        try:
            json_data = request.get_json()
            Asset.objects().latest(ticker=json_data['value']).delete()
            return {"message": "Last asset deleted"}, 200
        except (BadRequest, AttributeError):
            return {"error": "Some error occur"}, 418


def _convert_filters(args):
    filters = dict()
    field, op, value = args.values()
    if op:
        field = field + '__' + op
    filters[field] = value
    return filters
