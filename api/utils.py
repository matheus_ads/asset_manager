import math
from decimal import Decimal
from datetime import date


B3_LIQ_TAX = Decimal(0.000275)
B3_EMOL_TAX = Decimal(0.0000411)
NEW_EMOL_TAX = Decimal(0.00003125)
INCOME_TAX = Decimal(0.00005)


def truncate(f, n):
    return math.floor(f * 10 ** n) / 10 ** n


def calc_new_avg_price(old_avg: Decimal, old_qty: int, new_price: Decimal, new_qty: int, tax=0, emol=0) -> Decimal:
    """Calculate new weighted average price based on the old avg and qty
        params:
        old_avg: Decimal ex 145.14
        old_total: int ex 7
        new_price: Decimal ex 175.89
        new_qty: int ex 3 (not sum of old and new)
        return new avg with taxes or without
        return: new_avg: Decimal trunc by 2
    """
    old_total = old_avg * old_qty
    new_total = (new_price * new_qty) + (tax + emol)
    new_avg = (old_total + new_total) / (old_qty + new_qty)
    return Decimal(truncate(new_avg, 2))


def calc_liq_tax(quantity: int, price: Decimal) -> Decimal:
    """Calculate Liquidation Tax with two decimal places"""
    result = quantity * price * B3_LIQ_TAX
    return Decimal(truncate(result, 2))


def calc_emol_tax(quantity: int, price: Decimal, order_date: date) -> Decimal:
    """Calculate Emoluments with two decimal places"""
    emol_tax = _choose_tax_by_date(order_date)
    result = quantity * price * emol_tax
    return Decimal(truncate(result, 2))


def calc_income_tax(quantity: int, price: Decimal) -> Decimal:
    """Income tax already paid by broker, informed in bill"""
    income_tax = (quantity * price) * INCOME_TAX
    return Decimal(truncate(income_tax, 2))


def _choose_tax_by_date(order_date):
    """On the first day of 2020 B3 updates some taxes,
    so if the order was after this we will use the new value"""
    new_date = date(2020, 1, 1)
    return NEW_EMOL_TAX if order_date > new_date else B3_EMOL_TAX
